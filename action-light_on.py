#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
from hermes_python.hermes import Hermes
from hermes_python.ffi.utils import MqttOptions
from hermes_python.ontology import *
import requests
import io
import json

translation_table = {           #translation table to replace german umlauts
    "ä": "ae",
    "ö": "oe",
    "ü": "ue",
    "Ü": "Ue",
    "Ö": "Oe",
    "Ä": "Ae"
}

CONFIGURATION_ENCODING_FORMAT = "utf-8"
CONFIG_INI = "config.ini"

class SnipsConfigParser(configparser.SafeConfigParser):
    def to_dict(self):
        return {section : {option_name : option for option_name, option in self.items(section)} for section in self.sections()}

def read_configuration_file(configuration_file):
    try:
        with io.open(configuration_file, encoding=CONFIGURATION_ENCODING_FORMAT) as f:
            conf_parser = SnipsConfigParser()
            conf_parser.readfp(f)
            return conf_parser.to_dict()
    except (IOError, configparser.Error) as e:
        return dict()

def subscribe_intent_callback(hermes, intentMessage):
    conf = read_configuration_file(CONFIG_INI)
    action_wrapper(hermes, intentMessage, conf)


def action_wrapper(hermes, intentMessage, conf):
    openhab_port = conf['global'].get("openhab_server_port")                #getting openhab port from config.ini
    openhab_server = conf['global'].get("openhab_server_url")               #getting openhab server ip from config.ini
    room = intentMessage.slots.deviceLocation.first().value                 #gets room slot
    room_without = room.translate(str.maketrans(translation_table))         #replaces german umlauts
    room_item = room_without
    room_item += "_light"
    response = requests.get('http://{}:{}/rest/items/{}'.format(openhab_server, openhab_port, room_item))
    response_string = json.loads(response.text)
    item_type = response_string.get('type')
    if item_type == 'Switch':
        command = "ON"
    elif item_type == 'Dimmer':
        command = "100"
    elif item_type == 'Color':
        command = "0, 0, 100"
    res = requests.post('http://{}:{}/rest/items/{}'.format(openhab_server, openhab_port, room_item), command) #sending command to openhab
    if res.status_code == 202 or 200:
        result_sentence = "Ich schalte das Licht in {} ein".format(str(room))
    else:
        result_sentence = "Das Eitem {} gibt es nicht. Bitte erstelle in Openhab ein Eitem mit dem Namen: {} unterstrich leiht!"
    current_session_id = intentMessage.session_id
    hermes.publish_end_session(current_session_id, result_sentence)

if __name__ == "__main__":
    conf = read_configuration_file(CONFIG_INI)
    mqtt_server = conf['global'].get("mqtt_server")
    mqtt_opts = MqttOptions()
    with Hermes("{}:1883".format(mqtt_server)) as h:
        h.subscribe_intent("Paule:LampenAnSchalten", subscribe_intent_callback) \
.start()