Dies ist ein Fork von Lichtsteuerung by JKBecker
mit Python Skills für Openhab Rest API.

Parameter die konfiguriert werden müssen:
- mqtt_server: [mqtt server ip + port,standardmäßig localhost:1883]
- openhab_server_ip : [openhab ip]
- openhab_port: [openhab port, standardmäßig 8080]


Noch in Testphase !
Webseite: https://gitlab.com/faupau/snips-lichtsteuerung-openhab-skill

TODO:
Implementation von Szenen und Szenenwechsel